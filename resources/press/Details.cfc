<cfcomponent extends="taffy.core.resource" taffy_uri="/press/{id}" hint="Returns specific press article">
	<cffunction name="get" access="public" output="false">
		<!--- required --->
		<cfargument name="apiKey" type="string" required="true">
		<cfargument name="id" required="true" restargsource="Path" type="numeric" />

		<cfquery name="q">
			SELECT author, body, date_create, date_publish, filename, id, publicationname, sub_title, title, 
			CASE WHEN type = 1 THEN 'Article' ELSE 'Press Release' END type, url
			FROM press
			WHERE id = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.id#" />
			AND status = <cfqueryparam cfsqltype="cf_sql_integer" value="1" />
		</cfquery>

		<cfreturn representationOf(q).withStatus(200) />
	</cffunction>   
</cfcomponent>